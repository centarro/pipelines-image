# Pipelines Image #

This contains the Bitbucket Pipelines image used for testing Centarro Commerce code.

## Management

This belongs to the Centarro organization on Docker Hub: https://hub.docker.com/orgs/centarro

Public link: https://hub.docker.com/r/centarro/pipelines-image

## Building the image:

```
docker buildx build --platform linux/amd64 -t centarro/pipelines-image:latest --push .
```